# Maps for Dead in Thay

*Maps for Dead in Thay by Wizards of the Coast*

The D&D adventure **Dead in Thay** was released in Dungeon Magazine and then re-released in **Tales from the Yawning Portal**, and neither included maps usable for miniatures.

These are basic ("old school"-style) maps derived from the illustrations in the adventure in SVG format, making them suitable for online or printed play.

## Git portal

This project uses [git-portal](https://gitlab.com/slackermedia/git-portal), so it contains some symlinks to files not included in this repository for copyright reasons.
Official D&D material can be purchased from [D&D Beyond](http://dndbeyond.com) and [DMs Guild](http://dsmguild.com).
